.TH DKOPP 1 2010-10-01 "Linux" "Dkopp man page"
 
.SH NAME
   Dkopp - copy files to DVD or BD (Blue-ray) media
 
.SH SYNOPSIS
   \fBdkopp\fR [ \fB-job\fR | \fB-run\fR ] \fIjobfile\fR
   \fBdkopp\fR [ \fB-nogui\fR ] \fB-script\fR \fIscriptfile\fR
 
.SH DESCRIPTION
   Dkopp copies files to backup DVD or BD media. It supports full
   or incremental backups and full or incremental media verification.
 
.SH OVERVIEW
   Dkopp is a menu-driven GUI (GTK) program operating in its own window. 
   Dkopp copies files and directories specified in a job file to DVD or 
   BD media. Dkopp can copy all files to empty media (full copy), or 
   only new and modified files to previously used media (incremental). 
   Files and directories to include or exclude can be selected from the 
   file system hierarchy using a GUI navigator. Specifications are saved 
   in a job file which can be re-edited and re-used. Script files can 
   be run in batch mode using the \-nogui option. Dkopp can be used to
   select and restore files previously copied, and owner and permission
   data is also restored. The DVD/BD media can also be accessed with 
   file system tools like Nautilus.

   Dkopp supports the following functionalities:
   - Three backup modes: full, incremental, accumulate.
   - Three media verification modes: full, incremental, thorough.
   - Use write-once or re-writable DVD or BlueRay media (but not CD).
   - Report disk:backup differences in detail or summary form.
   - Select and restore files from a backup copy (or use drag and drop).
   - Search log files to find media where specified files are saved.
 
.SH OPTIONS
   Command line options:
   [ \fB-job\fR ] \fIjobfile\fR                 open job file for editing
   \fB-run\fR \fIjobfile\fR                     execute a job file
   [ \fB-nogui\fR ] \fB-script\fR \fIscriptfile\fR    execute a script file
 
.SH SEE ALSO
   The online user manual is available using the menu Help > contents.
   This manual explains Dkopp operation in great detail.
   
   Dkopp uses the batch programs \fBgrwoisofs\fR and \fBgenisoimage\fR.
   Dkopp is essentially a GUI front-end for these programs.
 
.SH AUTHORS
   Written by Mike Cornelison <mkornelix@gmail.com>
   https://kornelix.net

