dkopp (7.7-1) unstable; urgency=medium

  * New upstream release.
  * Bumped Standards-Version to 4.5.1.
  * Bumped debhelper compatibility level to 13.
  * Removed 02-makefile.patch. No longer applicable.
  * Updated other patches.
  * Added lshw as a dependency.
  * Added libclutter-gtk-1.0-dev as a build dependency.
  * Vcs-Git and Vcs-Browser updated to dkopp Salsa repository.
  * User guide is in text instead of HTML due to upstream change.

 -- Hill Ma <maahiuzeon@gmail.com>  Sat, 20 Feb 2021 12:12:31 -0800

dkopp (6.5-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use blkid instead of volname. (Closes: #787425)

 -- Chris Hofstaedtler <zeha@debian.org>  Tue, 05 May 2020 20:06:12 +0000

dkopp (6.5-1) unstable; urgency=medium

  * New upstream release.
  * Update 02-makefile.patch

 -- Leo Iannacone <l3on@ubuntu.com>  Fri, 06 Jun 2014 14:26:25 +0200

dkopp (6.4-1) unstable; urgency=medium

  * Imported Upstream version 6.4
  * Bump Standards-Version to 3.9.5
  * Refreshed old patches
  * Removed 03-fix-ftbfs-underlink.patch. Applied upstream.
  * Updated source reference uri in debian/copyright.

 -- Leo Iannacone <l3on@ubuntu.com>  Mon, 03 Mar 2014 20:09:40 +0100

dkopp (6.3.1-1) unstable; urgency=low

  * New upstream release.
  * Update watch file.
  * Update project homepage url.
  * Refreshed old patches.
  * Fix VCS URLs.
  * Use debhelper 9 for hardering.
  * Drop 03-fix-include-hurd.patch, now in upstream.
  * debian/dkopp.doc-base:
    + Fix index filename
  * 01-desktop_file.patch: added keywords.
  * new 03-fix-ftbfs-underlink.patch: Added missing pthread 
    lib to link, fixing FTBFS on armhf.
    Thanks to Daniel T Chen (closes: #713647).
      
 -- Leo Iannacone <l3on@ubuntu.com>  Tue, 09 Jul 2013 17:35:25 +0200

dkopp (6.2-1) unstable; urgency=low

  * Importe Upstream version 6.2 (Closes: #668484)
  * debian/patches:
    + removed 03-fix-include-gtk-as-FTBFS.patch, changes are 
      now in upstream
    + added 03-fix-include-hurd.patch, fixes FTBFS with hurd, 
      thanks to Cyril Roelandt (Closes: #670992)
    + refreshed old patches.
  * Bump standards version to 3.9.3:
    + update copyright format

 -- Leo Iannacone <l3on@ubuntu.com>  Wed, 23 May 2012 12:28:33 +0200

dkopp (6.0-2) unstable; urgency=low

  * Imported patch 03-fix-include-gtk-as-FTBFS.patch from Ubuntu,
    fixes FTBFS including gtk.h before pthread redefinitions.
  * Refreshed existing patches.
 
 -- Leo Iannacone <l3on@ubuntu.com>  Sat, 07 Jan 2012 11:51:53 +0100

dkopp (6.0-1) unstable; urgency=low

  * Imported Upstream version 6.0
  * Refresh 01-desktop_file.patch
  * Refresh 02-makefile.patch
  * Drop patch 03-nonlinux_ftbfs.patch. Changes are now in upstream
  * New maintainer (Closes: #650446)

 -- Leo Iannacone <l3on@ubuntu.com>  Sun, 11 Dec 2011 11:01:21 +0100

dkopp (5.9.2-1) unstable; urgency=low

  * New upstream release:
    - Compensation added for hung DVD/BD drive after growisofs is done.
      The drive can become unresponsive (ignores mount commands), but
      ejecting and reloading cures the problem and allows the verify
      operation to proceed.
  * Refresh 03-nonlinux_ftbfs.patch

 -- Alessio Treglia <alessio@debian.org>  Sat, 20 Aug 2011 10:48:21 +0200

dkopp (5.9.1-2) unstable; urgency=low

  * Fix FTBFS on kfreebsd (Closes: #637891) and set dependency on udev to
    linux-any only, credits to Cristoph Egger for the report and patch.

 -- Alessio Treglia <alessio@debian.org>  Tue, 16 Aug 2011 09:34:48 +0200

dkopp (5.9.1-1) unstable; urgency=low

  * New upstream bugfix release.
  * Refresh 02-makefile.patch.

 -- Alessio Treglia <alessio@debian.org>  Sat, 13 Aug 2011 11:53:44 +0200

dkopp (5.9-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Remove 03-manpage.patch, applied upstream.

 -- Alessio Treglia <alessio@debian.org>  Mon, 08 Aug 2011 13:02:27 +0200

dkopp (5.8-1) unstable; urgency=low

  * Imported Upstream version 5.8:
    - Growisofs always returns a bad status for Blue-Ray media, even though
      they are always OK (so far). The user message was changed to state that
      the media is likely OK and give the option to continue accordingly.
      The verify phase will determine the true media status afterwards.
  * Delete debian/dkopp.{1,manpages}, a manpage is now provided by upstream.
  * debian/{patches/*,rules}:
    - Let Makefile handle substitutions in the desktop file.
    - Let Makefile install the desktop file.
  * debian/patches/03-manpage.patch:
    - "-" must be escaped ("\-") to be interpreted as minus.
  * Depends on dvd+rw-tools.
  * Refresh patches.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sat, 07 May 2011 14:10:09 +0200

dkopp (5.7-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Alessio Treglia <alessio@debian.org>  Thu, 24 Feb 2011 12:39:59 +0100

dkopp (5.6-1) unstable; urgency=low

  * New upstream bugfix release.
  * Update manpage.
  * Refresh patches.

 -- Alessio Treglia <alessio@debian.org>  Sat, 11 Dec 2010 11:30:15 +0100

dkopp (5.5-2) unstable; urgency=low

  * Update desktop file.
  * Pass -Wl,--as-needed to the linker.
  * Fix the linking order to make GCC set LDFLAGS properly.

 -- Alessio Treglia <alessio@debian.org>  Tue, 09 Nov 2010 00:12:11 +0100

dkopp (5.5-1) unstable; urgency=low

  * New upstream release:
    - Bugfix: -script and -nogui failed to work if script is verify only.
    - Improved diagnostics and progress monitoring for -nogui option.
  * Add dependency on udev.
  * Add local-options file.
  * Add sign-tags = True in debian/gbp.conf
  * Add manpage.

 -- Alessio Treglia <alessio@debian.org>  Tue, 21 Sep 2010 10:18:13 +0200

dkopp (5.4-1) unstable; urgency=low

  * Initial release (Closes: #594321).

 -- Alessio Treglia <alessio@debian.org>  Tue, 31 Aug 2010 00:56:09 +0200
